{-# OPTIONS_GHC -Wall #-}

module LogAnalysis where
import Log
import Data.List (isInfixOf)

{-

- Files you will need: Log.hs, error.log, sample.log
- Files you should submit: LogAnalysis.hs


The error.log file has lines in the following format:

		I 147 mice in the air, I’m afraid, but you might catch a bat, and
		E 2 148 #56k istereadeat lo d200ff] BOOTMEM

- ’I’ means informational messages,
- ’W’ means warnings, and
- ’E’ means errors.

If E, it has an integer which indicates the severity of the error:
- 1 means low
- 100 means high

After these two pieces of information is a simple integer timestamp.

To help you get started, use these data types,
which are defined in the Log module:

data MessageType = Info
				| Warning
				| Error Int
	deriving (Show, Eq)

type TimeStamp = Int

data LogMessage = LogMessage MessageType TimeStamp String
				| Unknown String
	deriving (Show, Eq)

-}


-- Excercise 1 (2 parts)
{-

The ﬁrst step is ﬁguring out how to parse an individual message.
Deﬁne a function which parses an individual line from the log ﬁle.

To test your function, use the testParse function in the
Log module. Other functions which may be useful include
	
	lines, words, unwords, take, drop, and (.).

-}

-- 1)
parseMessage :: String -> LogMessage
parseMessage messageLine = case (words messageLine) of
		("I" : timestamp : message)
			-> LogMessage (Info) (read timestamp) (unwords message)
		("W" : timestamp : message)
			-> LogMessage (Warning) (read timestamp) (unwords message)
		("E" : errorNumber : timestamp : message)
			-> LogMessage (Error (read errorNumber)) (read timestamp) (unwords message)
		logLine@(_ : _) -> Unknown (unwords logLine)
		[] -> Unknown ""


-- Use these to test result.
--parseMessage "E 2 562 help help"
-- == LogMessage (Error 2) 562 "help help"

--parseMessage "I 29 la la la"
-- == LogMessage Info 29 "la la la"

--parseMessage "This is not in the right format"
-- == Unknown "This is not in the right format"

-- 2)
-- Now parse and entire log file.
parse :: String -> [LogMessage]
parse fileContents = map parseMessage (lines fileContents)

-- Use this to test result.
-- Use x function, read y lines from z file.
--testParse parse 10 "error.log"



{-
To put the logs in order, use a binary search tree.
Use this data type, defined in this homework's Log module:

data MessageTree = Leaf
				| Node MessageTree LogMessage MessageTree

MessageTree should be sorted by timestamp:
  the timestamp of a LogMessage in any Node should be greater
  than all timestamps of any LogMessage in the left subtree,
  and less than all timestamps of any LogMessage in the right child.

Don't put Unknown messages in MessageTree, because they have no timestamp.
-}


-- Exercise 2
{-

Deﬁne a function which inserts a LogMessage into a sorted MessageTree.

Assume the MessageTree argument is already sorted.
It must produce a new sorted MessageTree which includes the new LogMessage.
Note: If LogMessage is Unknown, return the original MessageTree.

-}

insert :: LogMessage -> MessageTree -> MessageTree
-- For completeness.
insert _ originalTree@(Node _ (Unknown _) _)
		= originalTree
-- Don't allow Unknown into the tree.
insert (Unknown _) originalTree
		= originalTree
-- If insert LogMessage into Leaf.
insert logMessage Leaf
		= Node Leaf logMessage Leaf
-- If insert LogMessage into Tree, choose left or right side, based on TimeStamp.
insert logMessage@(LogMessage _ newTime _) (Node leftTree originalMessage@(LogMessage _ nodeTime _) rightTree)
		= if (newTime < nodeTime)
			then Node (insert logMessage leftTree) originalMessage rightTree--then insert logMessage leftTree
			else Node leftTree originalMessage (insert logMessage rightTree)--else insert logMessage rightTree



-- My tests.
emptyTree :: MessageTree
emptyTree = Leaf
infoMessage, errorMessage, errorMessage2, infoMessage2, unknownMessage :: LogMessage
infoMessage = LogMessage Info 1 "Message contents here."
errorMessage = LogMessage (Error 5) 15 "Error messages."
errorMessage2 = LogMessage (Error 6) 20 "Error messages."
infoMessage2 = LogMessage Info 30 "Message contents here 2."
unknownMessage = Unknown "blah blah blah"

tree1, tree2, tree3, tree4, tree5 :: MessageTree
tree1 = insert infoMessage emptyTree
tree2 = insert infoMessage2 tree1
tree3 = insert errorMessage tree2
tree4 = insert errorMessage2 tree3
tree5 = insert unknownMessage tree4


-- Exercise 3
{-

Build a complete MessageTree from a list of messages.
Specifically, deﬁne a function which builds up a MessageTree
containing the messages in the list, by successively inserting
the messages into a MessageTree (beginning with a Leaf).

-}

build :: [LogMessage] -> MessageTree
build [] = Leaf
build (message : messages) = insert message (build messages)




-- Exercise 4
{-

Finally, deﬁne the function which takes a sorted MessageTree and
produces a list of all the LogMessages it contains, sorted by timestamp from smallest to biggest.

(This is known as an in-order traversal of the MessageTree.)

With these functions, we can now remove Unknown messages
and sort the well-formed messages using an expression such as:

inOrder (build tree)

-}

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf
		= []
inOrder (Node leftTree logMessage rightTree)
		= (inOrder leftTree) ++ [logMessage] ++ (inOrder rightTree)



-- Exercise 5
{-

Now that we can sort the log messages, the only thing
left to do is extract the relevant information. We have decided that
“relevant” means “errors with a severity of at least 50”.
Write a function which takes an unsorted list of LogMessages, and returns a list of the
messages corresponding to any errors with a severity of 50 or greater,
sorted by timestamp. (Of course, you can use your functions from the
previous exercises to do the sorting.)

-}


-- Method 1: Use temporary functions.
---------
isSevereError :: LogMessage -> Bool
isSevereError (LogMessage (Error severity) _ _)
		= severity >= 50
isSevereError _
		= False

getMessage :: LogMessage -> String
getMessage (LogMessage _ _ message) = message
getMessage _ = ""

whatWentWrong' :: [LogMessage] -> [String]
whatWentWrong' messages = map getMessage (inOrder (build filteredMessages))
		where
			filteredMessages = filter isSevereError messages


-- Method 2: Use list comprehensions.
---------
whatWentWrong :: [LogMessage] -> [String]
whatWentWrong messages = [ message | LogMessage _ _ message <- inOrder (build filteredMessages) ]
		where
			filteredMessages = [x | x@(LogMessage (Error severity) _ _) <- messages, severity >= 50]


-- Use this to test result.
--testWhatWentWrong parse whatWentWrong "error.log"



-- Exercise 6 (Optional)
{-

For various reasons we are beginning to suspect that the recent mess was
caused by a single, egotistical hacker. Can you ﬁgure out who did it?

-}


-- See what happened right before the mustardwatch was opened.
whatWentWrong'' :: [LogMessage] -> [String]
whatWentWrong'' messages = [ message | LogMessage _ timestamp message <- inOrder (build filteredMessages)]
		where
			filteredMessages = [x | x@(LogMessage _ timestamp _) <- messages, timestamp > 100 && timestamp < 130]
			--filteredMessages = [x | x@(LogMessage Warning timestamp _) <- messages]


-- Look for all references to opening the mustardwatch.
whatWentWrong''' :: [LogMessage] -> [String]
whatWentWrong''' messages = [ message | LogMessage _ timestamp message <- inOrder (build filteredMessages)]
		where
			filteredMessages = [x | x@(LogMessage _ _ message) <- messages, "mustard" `isInfixOf` message || "Mustard" `isInfixOf` message || "watch" `isInfixOf` message || "open" `isInfixOf` message]


