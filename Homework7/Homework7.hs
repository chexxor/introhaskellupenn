{-# OPTIONS_GHC -Wall #-}

module Homework7 where
import Data.Monoid
import JoinList

-- Introduction: The First Word Processor
{-

As everyone knows, Charles Dickens was paid by the word. What 
most people don't know, however, is the story of you, the trusty
programming assistant to the great author.

We are developing a primitive word processor for Dickens. We
have built a a word processer which counts total number of words
while it is being edited. We want to improve it.

## Editors and Buffers ##

We have a working UI, saved in the `Editor.hs` file. This module
defines functionality for working with documents which implement
the `Buffer` type class, which is defined in the `Buffer.hs` file.
Look at this module to see which operations a `Buffer` must support.
The intention is to separate the UI from the back-end representation.

The editor interface is as follows:

- v = view the current location
- n - next line
- p - previous line
- l - load a file
- e - edit current line
- q - quit
- ? - show commands

The old back-end used a `String` type to represent the whole document.
This back-end is saved in the `StringBuffer.hs` file. Performance is poor
because reporting the word count requires traversing every single character
in the document, every time the score is shown.

We decided to fix this performance problem by implementing a light-weight,
tree-like structure to hold the data and its metadata. This data structure
is called a "join-list". Here's its type signature.


		data JoinListBasic a =
				Empty
				| Single a
				| Append (JoinListBasic a) (JoinListBasic a)

If we use this data structure, appending new data is very fast, because
joining two `JoinListBasic` types is simply using the Append constructor.
For example:

		jlbToList :: JoinListBasic a -> [a]
		jlbToList Empty = []
		jlbToList (Single a) = [a]
		jlbToList (Append l1 l2) = jlbToList l1 ++ jlbToList l2

This data structure makes sense for text editing applications because
it breaks the data into pieces which can be processed individually,
rather than character-by-character walking the entire document.

## Monoidally Annotated Join-Lists ##

Use this definition of a Join-List for this assignment, saved
into a file called `JoinList.hs`.

		data JoinList m a =
				Empty
				| Single m a
				| Append m (JoinList m a) (JoinList m a)
			deriving (Eq, Show)

The `m` parameter will track monoidal annotations to the structure. How?
The annotiation at the root of a JoinList will always be equal to all
annotations on the `Single` nodes. `Empty` nodes have an annotation value
of `mempty`.

Here is an example of a data structure we want to build:

		Append (Product 210)
			(Append (Product 30)
				(Single (Product 5) 'y')
				(Append (Product 6)
					(Single (Product 2) 'e')
					(Single (Product 3) 'a')))
			(Single (Product 7) 'h')

The multiplicative monoid is being used, so each `Append` node
stores the product of all annotations below it. How is this better
than computing totals on the previous back-end, a String? This caches
the sub-computations. If we change an annotation, such as "y", we don't
need to recalculate the "e" and "a" nodes.

-}


-- Exercise 1: Join-List Operations
{-

How do we append two `JoinList`s?

1) Write an append function for `JoinList`s which yields a new
`JoinList` whose monoidal annotation is derived from the annotations of
its two arguments. Use the following function signature.

		(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a

The following function may be helpful. It gets the annotation at the
root of a `JoinList`.

		tag :: Monoid m => JoinList m a -> m

-}

-- 1.a) Make an append function.
-- TODO: Finish this.
(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
(+++) Empty Empty
	= Empty
(+++) Empty r
	= r
(+++) l Empty
	= l
(+++) (Single m1 a1) (Single m2 a2)
	= (Append (m1 * m2))


-- TODO: Finish this.
tag :: Monoid m => JoinList m a -> m
tag Empty
	= mempty
tag (Single m a)
	= m
tag (Append m (JoinList m1 a1) (JoinList m2 a2))
	= tag m1

-- TODO: Maybe I should write a fold function for `JoinList`.


