{-# OPTIONS_GHC -Wall #-}

module Homework6 where
import Data.List (foldl', find)

-- Introduction
{-

The Fibonacci numbers F(n) are defined as the sequence of integers,
beginning with 0 and 1, where every integer in the sequence is the
sum of the previous two. That is

		F 0 = 0
		F 1 = 1
		F n = F(n-1) + F(n-2) where n >= 2

For example, the first fifteen Fibonacci numbers are
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, ...

-}



-- Exercise 1
{-

a) Translate the above definition of Fibonacci numbers directly into a
recursive function definition so that `fib n` computes the nth
Fibonacci number F(n). The function type signature should be:

		fib :: Integer -> Integer

b) Now use `fib` to define a function which represents the infinite list
of all Fibonacci numbers. The function signature should be:

		fibs1 :: [Integer]

(Hint: You can write the list of all positive integers as [0..].)

Try evaluating `fibs1` at the ghci prompt. `fib` is ridiculously slow.
Although it is a good way to define the Fibonacci numbers, it is not
a very good way to compute them. Exercise 2, we improve this.

-}

-- 1.a) Create Fibonacci function.
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib(n-1) + fib(n-2)

-- 1.b) Create infinite list of Fibonacci numbers.
fibs1 :: [Integer]
fibs1 = map fib [0..]


-- Exercise 2
{-

Create an implementation of Fibonacci such that computing
the first n elements requires O(n) addition operations.
Be sure to use standard recursion patterns from Prelude
as appropriate.

-}

-- Notes:
-- Let's look at the recursive part, because it is the only complex piece.
-- Because of laziness, it will build a huge list of numbers, then evaluate them.
-- How do we solve laziness in Haskell?
-- Use the `seq` function?
--   http://www.haskell.org/haskellwiki/Seq
--   `seq` evaluates first arg, then performs second arg.
-- Change algorithm?
--   fib(n-1) and fib(n-2) seem similar...
--   fib [5] == fib [fib 5-1, fib 4-1] == fib [3, 2, 2, 1] == fib [2, 1, 1, 1, 1] == fib [1, 1, 1, 1, 1]

-- Using foldl' works, but we still generate a huge list of numbers.
fibFold' :: Integer -> Integer
fibFold' 0 = 0
fibFold' 1 = 1
fibFold' n = foldl' (+) 0 (fibList (n))
fibList :: Integer -> [Integer]
fibList 0 = []
fibList 1 = [1]
fibList n = fibList (n-1) ++ fibList(n-2)


-- Can we use scanl? It builds a list.
-- It takes the second argument and the first item of the list and
--   applies the function to them, then feeds the function with this result
--   and the second argument, and so on

-- scanl (+) 0 [1..n]
-- (+) 0    0 == 0
-- (+) that 1 == 1
-- (+) that 2 == 3
-- (+) that 3 == 6
-- scanl (+) 0 [1:1:2]
-- (+) 0    a == 1, a = 1
-- (+) that b == 1, b = 0
-- (+) that c == 2, c = 1
-- (+) that d == 3, d = 1
-- (+) that e == 5, e = 2
-- (+) that f == 8, f = 3
-- (+) that g == 13, g = 5
-- (+) that h == 21, h = 8

-- I choose input to get desired output.
fibScanTest' :: Integer -> [Integer]
fibScanTest' _ = scanl (+) 0 (1:0:1:1:2:3:5:8:13:21:[]) -- Input looks like Fibonacci...
--fibScan' n = scanl (+) 0 (1:(fibScanTest' 5)) -- Try input the Fibonacci...
-- == [0,1,1,2,3,5,8,13,21,34,55,89,144] -- Nice.
fibScanTest2' :: Integer -> [Integer]
fibScanTest2' _ = scanl (+) 0 (1:(fibScanTest2' 5)) -- Try this...
-- == [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,...]
-- Get so many Fibonacci numbers, really fast. Numbers seem correct. But I'm not using `n`.
fibScanTest3' :: Integer -> [Integer]
fibScanTest3' n = scanl (+) 0 (1:(fibScanTest3' n)) -- Try this...
-- == <Same as above> -- Maybe I can just use this with no arguments.
fibScan' :: [Integer]
fibScan' = scanl (+) 0 (1:fibScan') -- Try this...


fibs2 :: [Integer]
fibs2 = fibScan'



-- Exercise 3: Streams
{-

The usual list type represents lists that *may be* infinite, but may also
have some finite length. We can be more explicit about infinite lists
by defining a type `Stream`, which represents lists that *must be* infinite.

In particular, streams are like lists but with only a "cons" constructor.
Whereas the list type has two constructors, [] and (:), a stream is simply
defined as (:), which is an element followed by a stream.

a) Define a data type of polymorphic streams, `Stream`.

b) Write a function to convert a `Stream` to an infinite list,

		streamToList :: Stream a -> [a]

c) To test your Stream functions, it will be useful to have an instance
of Show for Streams. However, if you simply use `deriving Show`,
the resulting instance will try to print an entire Stream, which is infinite, so
it will never finish. Instead, you should make your own instance of Show for
Stream, which works by showing only some prefix of a stream, such as the
first 20 elements.

		instance Show a => Show (Stream a) where
			show ...

-}


-- 3.a) Define the Stream type.
data Stream a = a :< [a]

-- 3.b) Convert a Stream to an infinite list.
streamToList :: Stream a -> [a]
streamToList (a :< as) = a:as

-- 3.c) Make Stream show-able.
instance Show a => Show (Stream a) where
	--show (x :< xs) = show (x : take 20 xs)
	show = show . take 20 . streamToList


-- Exercise 4: Tools for Streams
{-

Let’s create some simple tools for working with Streams.

a) Write a function which generates a stream containing infinitely
many copies of the given element.

		streamRepeat :: a -> Stream a

b) Write a function which applies a function to every element of a Stream.

		streamMap :: (a -> b) -> Stream a -> Stream b

c) Write a function which generates a Stream from a "seed" of type a.
Its first element is the original value, and each following element
is applied an "unfolding rule", which specifies how to transform the seed
into a new seed, to be used for generating the rest of the stream.

		streamFromSeed :: (a -> a) -> a -> Stream a

-}

-- 4.a) Write a streamRepeat function.
streamRepeat :: a -> Stream a
streamRepeat n = (n :< repeat n)

-- 4.b) Write a streamMap function.
streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (x :< xs) = f x :< map f xs

-- 4.c) Write a streamFromSeed function.
streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f n = n :< iterate f (f n)



-- Exercise 5: Working with Streams
{-

Now that we have some tools for working with streams, let's create a few.

a) Define the stream of the natural numbers: [0,1,2,...]

		nats :: Stream Integer

b) Define the stream which corresponds to the "ruler" function,
where every nth element is the largest power of 2 which evenly divides n:

		ruler :: Stream Integer
		== [0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, ...]


Hint: Define a function `interleaveStreams`, which alternates the elements
from two streams. Can you use this function to implement ruler in a
clever way that does not have to do any divisibility testing?

-}

-- 5.a) Create a Stream of natural numbers.
nats :: Stream Integer
nats = streamFromSeed (+1) 1

-- 5.b) Create a Stream of "ruler" numbers.
ruler :: Stream Integer
ruler = streamMap rulerVal nats


rulerVal :: Integer -> Integer
rulerVal x
	| odd x     = 0
	| otherwise = case greatestDivisibleTwo x of
		Just (p,_) -> p
		Nothing    -> 0


greatestDivisibleTwo :: Integer -> Maybe (Integer, Integer)
greatestDivisibleTwo x = find (\(_,v) -> divisible x v) $ reverse $ powsOfTwoList x

divisible :: Integer -> Integer -> Bool
divisible a b = 0 == mod a b

powsOfTwoStream :: Stream Integer
powsOfTwoStream = streamFromSeed (*2) 1

powsOfTwoList :: Integer -> [(Integer, Integer)]
--powsOfTwoList m = takeWhile (\(_,v) -> v <= m) $ iterate (\(p,v) -> (p+1, v*2)) (1,2)
powsOfTwoList m = [ (i, j) | i <- [1..m], let j = 2^i, j <= m ]



-- Challenge: Implement without using divisibility testing.

-- Idea: recursive interleaveStreams?

interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams (a :< as) (b :< bs)
	= a :< (b : interleaveLists as bs)

interleaveLists :: [a] -> [a] -> [a]
interleaveLists [] bs
	= (bs)
interleaveLists as []
	= (as)
interleaveLists (a:[]) (b:bs)
	= (a:b:bs)
interleaveLists (a:as) (b:[])
	= (a:b:as)
interleaveLists (a:as) (b:bs)
	= (a:b:(interleaveLists as bs))

{-
-- Interesting...

> let a = streamRepeat 0;
	b = streamRepeat 1;
	c = streamRepeat 2;
	d = streamRepeat 3;
	e = streamRepeat 4 in
		interleaveStreams a $ interleaveStreams b $ interleaveStreams c $ interleaveStreams d $ e
== [0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2]

> ruler
== [0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2]
-}

ruler' :: Stream Integer
ruler' = rulerInterleave 0 10

rulerInterleave :: Integer -> Integer -> Stream Integer
rulerInterleave _ 0 = streamRepeat 0
rulerInterleave n stop
	| n /= stop  = interleaveStreams (streamRepeat (n)) (rulerInterleave (n+1) stop)
	| otherwise  = interleaveStreams (streamRepeat (n)) (streamRepeat (n+1))



-- TODO: But I'm tired of this.

-- Exercise 6: Fibonacci Numbers via Matrices


-- Exercise 7: Matrices




