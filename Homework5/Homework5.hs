{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Homework5 where
import qualified ExprT
import Parser (parseExp)
import qualified StackVM (StackExp(..))
import           StackVM (Program)
import qualified Data.Map as M

-- Introduction
{-

Haskell’s type classes provide "ad-hoc polymorphism", that is,
choosing an algorithm based on the type of an input.

These exercises explore another interesting application
of type classes: domain-specific languages.

Expressions

You’ve been asked to program your company’s new blockbuster product:
a calculator. Extensive focus group analysis has revealed that
people want their calculator to add and multiply integers, nothing more.

Your boss has already started by modeling the domain with the
following data type to represent arithmetic expressions:

		data ExprT = Lit Integer
		            | Add ExprT ExprT
		            | Mul ExprT ExprT
		  deriving (Show, Eq)

This type is capable of representing expressions involving integer
constants, addition, and multiplication. For example, the expression
(2 + 3) x 4 would be represented by this value:

		Mul (Add (Lit 2) (Lit 3)) (Lit 4).

Your boss has already provided the definition of `ExprT` in 'ExprT.hs',
so as usual you just need to add `import ExprT` to the top of your file.
However, this is where your boss got stuck.

-}


-- Exercise 1: Calculator Version 1
{-
a) Write Version 1 of the calculator: an evaluator for ExprT named `eval`.
For example:

		eval (Mul (Add (Lit 2) (Lit 3)) (Lit 4)) == 20		
-}

-- 1.a)
eval :: ExprT.ExprT -> Integer
eval (ExprT.Lit x)   = x
eval (ExprT.Add x y) = eval x + eval y
eval (ExprT.Mul x y) = eval x * eval y

-- > eval (Mul (Add (Lit 2) (Lit 3)) (Lit 4))
-- 20


-- Exercise 2: Add a String Interface
{-
The UI department has  developed the front-facing
user-interface: a parser that handles the textual representation of the
selected language.

They have sent you the module Parser.hs, which
exports parseExp, a parser for arithmetic expressions. If you pass
the constructors of ExprT to it as arguments, it will convert Strings
representing arithmetic expressions into values of type ExprT. For
example:

		> parseExp Lit Add Mul "(2+3)*4"
		Just (Mul (Add (Lit 2) (Lit 3)) (Lit 4))
		> parseExp Lit Add Mul "2+3*4"
		Just (Add (Lit 2) (Mul (Lit 3) (Lit 4)))
		> parseExp Lit Add Mul "2+3*"
		Nothing

a) Use the `parseExp` function to write a function named `evalStr`,
which evaluates arithmetic expressions given as a String.
It produces `Nothing` for bad inputs and `Just n` for good inputs.
-}

-- 2.a)
evalStr :: String -> Maybe Integer
evalStr x = case parseExp ExprT.Lit ExprT.Add ExprT.Mul x of
	Just n -> Just (eval n)
	Nothing -> Nothing

-- > evalStr "(2+3)*4"
-- Just 20
-- > evalStr "(2+3)*"
-- Nothing



-- Exercise 3: Define Calculator Properties in a Type Class
{-
Good news! Early customer feedback indicates that people really
do love the interface! Unfortunately, there is disagreement over
exactly how the calculator should perform calculations.

The problem is that `ExprT` is rather inflexible, which is a barrier
to selling to diverse demographics. We should abstract away the
properties of `ExprT` with a type class. That is, make a class which
describes the essential properties of an `ExprT`, then make `ExprT`
merely one type in this class.

Tasks:

a) Create a type class named `Expr`. It should have three methods, `lit`, `add`,
and `mul`. These represent the essential abilities of our calculator.
Make `ExprT` an instance of this type.
If used in GHCI, its behavior should be:

		> mul (add (lit 2) (lit 3)) (lit 4) :: ExprT
		Mul (Add (Lit 2) (Lit 3)) (Lit 4)

Think carefully about the `lit`, `add`, and `mul` function's types. It
may be helpful to consider the `ExprT` constructors, which
you can see by typing `:t Lit` in GHCI.

b) Add `ExprT` to the `Expr` type class.


-----

Remark: Look at the type of an `ExprT` expression:

		> :t mul (add (lit 2) (lit 3)) (lit 4)
		Expr a => a

What does this mean? `Expr` is a type class, which means that the value
of this expression could be any instance of that type class.
Actually, the functions in this expression are part of `ExprT`,
specifically, so the type should reflect this.

One way to force a type is by using a type signature, as in the above example.
Another way is by using the expression as part of some larger
expression. Its context determines its type.

For example, we may write a function `reify` as follows:

		reify :: ExprT -> ExprT
		reify = id

It may look like `reify` does nothing, but its real purpose
is to create a new context to constrain its argument's type.
We can use this to choose a specific instance of a type class in GHCI.

		-- Explicitly change type to ExprT.
		reify $ mul (add (lit 2) (lit 3)) (lit 4)
-}

-- 3.a) Create `Expr` type class.
--    It should have three methods: `lit`, `add`, `mul`.
class Expr a where
	lit :: Integer -> a
	add :: a -> a -> a
	mul :: a -> a -> a

-- 3.b) Add `ExprT` to the `Expr` type class.
instance Expr ExprT.ExprT where
	lit n   = ExprT.Lit n
	add a b = ExprT.Add a b
	mul a b = ExprT.Mul a b

-- > mul (add (lit 2) (lit 3)) (lit 4) :: ExprT
-- Mul (Add (Lit 2) (Lit 3)) (Lit 4)

-- Note: If we don't use " :: ExprT", the compiler
--   can not find our implementation. Why? The functions
--   are attached to a specific type. In GHCI, this expression
--   has no defined types, so the compiler can not begin to
--   guess where the function's are implemented.


-- Exercise 4: Create Custom Calculators
{-
The marketing department has heard how flexible the calculator is
and has promised custom calculators to some big clients.

Everyone loves the new interface, but everyone disagrees on meaning of
`lit`, `add`, and `mul`. When we wrote `ExprT`, we thought that
addition and multiplication of integers was pretty well-defined.
Well, now some big clients want customized calculators with behaviors
which suit their specialized business needs.

With the `Expr` type class, we can now write arithmetic expressions
once and have them interpreted in various ways just by
using them with various types.

Make instances of `Expr` for each of the following types:

- a) Integer - unchanged
- b) Bool - every literal value <= 0 is interpreted as False,
		and all Integers > 0 are interpreted as True.
		"addition" is logical "OR", "multiplication" is logical "AND".
- c) MinMax - "addition" is interpreted as the `max` function,
		"multiplication" is the min function.
- d) Mod7 - all values should be in the range 0...6, and
		all arithmetic is done modulo 7; for example, 5 + 3 = 1.

The last two variants work with `Integer` types internally,
but to provide different instances, wrap them in "newtype" wrappers.
These are used just like the data constructors we've seen before:
-}
newtype MinMax = MinMax Integer deriving (Eq, Show)
newtype Mod7 = Mod7 Integer deriving (Eq, Show)
{-
Once complete, the following code should demonstrate the
correctness of our family of calculators:
-}
testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"
testInteger :: Maybe Integer
testInteger = testExp
testBool :: Maybe Bool
testBool = testExp
testMM :: Maybe MinMax
testMM = testExp
testSat :: Maybe Mod7
testSat = testExp
{-
Try printing out each of those tests in GHCI to see if things are
working.

Isn't it great how easily we can add new semantics for
the same syntactic expression?

You must complete Exercise 5 or Exercise 6, or both.
-}

-- 4.a) Integer Calculator
instance Expr Integer where
	lit n   = n
	add a b = a + b
	mul a b = a * b

-- > testInteger
-- Just (-7)

-- 4.b) Bool
instance Expr Bool where
	lit n   = if n <= 0 then False else True
	add a b = a || b
	mul a b = a && b
-- > testBool
-- Just True

-- 4.c) MinMax
instance Expr MinMax where
	lit n = MinMax n
	add (MinMax a) (MinMax b) =
			MinMax (min a b)
	mul (MinMax a) (MinMax b) =
			MinMax (max a b)
-- > testMM
-- Just (MinMax 3)

-- 4.d) Mod7
instance Expr Mod7 where
	lit n  = Mod7 (n `mod` 7)
	
	add (Mod7 a) (Mod7 b) =
		Mod7 (mod7 $ (+) am bm)
			where
				mod7 = (`mod` 7)
				-- all values should be in the range (0...6)
				am = mod7 a
				bm = mod7 b
	
	mul (Mod7 a) (Mod7 b) =
		Mod7 (mod7 $ (*) am bm)
			where
				mod7 = (`mod` 7)
				-- all values should be in the range (0...6)
				am = mod7 a
				bm = mod7 b
-- > testSat
-- Just (Mod7 0)

-- Checking:
-- (3 * -4) + 5 -- (`mod` 7) 3 == 3, (`mod` 7) (-4) == 3
-- (3 *  3) + 5 -- (`mod` 7) 9 == 2
-- 2 + 5        -- (`mod` 7) 7 == 0
-- 0



-- Exercise 5
{-
The folks down in hardware have finished our new custom CPU,
so we’d like to target that from now on.

You need to write a version of your calculator which will emit
a stack-based assembly language for the new processor.

The hardware group has provided you with `StackVM.hs`, which
is a software simulation of the custom CPU. The CPU supports six
operations, as embodied in the `StackExp` data type:

		data StackExp = PushI Integer
					| PushB Bool
					| Add
					| Mul
					| And
					| Or
					  deriving Show
		type Program = [StackExp]

`PushI` and `PushB` push values onto the top of the stack, which can
store both Integer and Bool values. `Add`, `Mul`, `And`, and `Or` each pop
the top two items off the top of the stack, perform the appropriate
operation, and push the result back onto the top of the stack. For
example, executing the following program will result in a stack
holding True on the bottom, and 18 on top of that.

			[PushB True, PushI 3, PushI 6, Mul]
			== [PushB True, PushI 18]

If there are not enough operands on top of the stack,
or if an operation is performed on operands of the wrong type,
the processor will melt into a puddle of silicon goo.
For a more precise specification of the capabilities and
behavior of the custom CPU, consult the
reference implementation provided in `StackVM.hs`.

Your task is to implement a compiler for arithmetic expressions:

a) Simply create an instance of the `Expr` type class for `Program`, so that
arithmetic expressions can be interpreted as compiled programs.

		stackVM exp == Right [IVal exp]
		-- where exp :: Expr a => a

Note: to make an instance for `Program`, which is a type synonym,
you must enable the `TypeSynonymInstances` language extension,
which you can do by adding the following to the file's first line.

		{\-# LANGUAGE TypeSynonymInstances #-\}

b) Finally, put together the pieces to create a function named `compile`,
which takes Strings representing arithmetic expressions
and compiles them into programs that can be run on the custom CPU.
-}

-- Problem with types I discovered:
-- Program == [StackExp], so we can write:
--   1) `instance Expr Program`
--   2) `instance Expr [StackExp]`
-- These type represent the same thing, which means two disagreeing
--   implementations might be added. The compiler should only find one.
-- Solution A: Add this flag: {-# LANGUAGE FlexibleInstances #-}
--   to tell the compiler "Don't worry so much, we will watch for this
--   problem in this module."
-- Solution B: Use a wrapper type to help the compiler differentiate
--   these two identical types: `newtype Program' = Program' [StackVM.StackExp]`
-- An explanation of another problem here:
--   http://stackoverflow.com/questions/8633470/illegal-instance-declaration-when-declaring-instance-of-isstring#answer-8663534
-- I chose Solution A.

-- 5.a) Create instance of `Expr` type class for `Program` type.

------ Notes ------
-- Expected behavior:
--  > compile "(3 * -4)"
--  Just [PushI 3,PushI (-4),Mul]
--  > compile "(3 * -4) + 5"
-- Should be == Just [PushI 3, PushI -4, Mul, PushI 5, Add]

-- What does `add [Program] [Program]` mean?
-- Right side should be new operation,
-- Left side should be parsed operation stack.
-- Like this:
-- > add [StackVM.PushI 3, StackVM.PushI (-4), StackVM.Mul] [StackVM.PushI 5]
-- == [PushI 3,PushI (-4),Mul,PushI 5,Add]
------ Notes ------

instance Expr StackVM.Program where
	lit n   = [StackVM.PushI n]

	add expr ((StackVM.PushI n) : _)
			= expr ++ [StackVM.PushI n, StackVM.Add]
	add _ _
			= []

	mul expr ((StackVM.PushI n) : _)
			= expr ++ [StackVM.PushI n, StackVM.Mul]
	mul _ _
			= []

-- 5.b) Create `compile` function
compile :: String -> Maybe Program
compile = parseExp lit add mul



-- Exercise 6
{-

Some users of your calculator have requested the ability to give
names to intermediate values and then reuse these stored values
later.

To enable this, you first need to add the ability to contain variables
for arithmetic expressions.

a) Create a new type class `HasVars a` which contains a
single method `var :: String -> a`. Thus, types which are
instances of `HasVars` have some notion of named variables.

b) Then create a new data type `VarExprT` which is the same
as `ExprT` but with an extra constructor for variables. Make `VarExprT`
an instance of both `Expr` and `HasVars`. You should now be able to
write things like

		> add (lit 3) (var "x") :: VarExprT

But we can’t stop there: we want to be able to interpret
expressions containing variables, given a suitable mapping from variables
to values. For storing mappings from variables to values, you should
use the `Data.Map` module, like this:

		import qualified Data.Map as M

The qualified import means that you must prefix `M.` whenever you refer
to things from Data.Map. This is standard practice, because `Data.Map`
exports many functions which overlap with Prelude function names.

c) Implement the following instances:

		instance HasVars (M.Map String Integer -> Maybe Integer)
		instance Expr (M.Map String Integer -> Maybe Integer)

The first instance says that variables can be interpreted as functions
from a mapping of variables to Integer values to (possibly)
Integer values. It should work by looking up the variable in the mapping.

The second instance says that these same functions can be interpreted
as expressions (by passing along the mapping to subexpressions and
combining results appropriately).

Note: To write these instances you will need to enable the `FlexibleInstances` language
extension by putting the following line at the top of your file.

		{\-# LANGUAGE FlexibleInstances #-\}
-}


-- 6.a) Create a `HasVars` type class.

class HasVars a where
	var :: String -> a

-- 6.b) Create a new data type `VarExprT`. Create instances for `Expr` and `HasVars`.

data VarExprT = Lit Integer
           | Add VarExprT VarExprT
           | Mul VarExprT VarExprT
           | Var String
  deriving (Show, Eq)

instance Expr VarExprT where
	lit n   = Lit n
	add a b = Add a b
	mul a b = Mul a b

instance HasVars VarExprT where
	var x = Var x

-- Test desired behavior.
-- > add (lit 3) (var "x") :: VarExprT
-- Add (Lit 3) (Var "x")



-- 6.c) Create instance of HasVars for a variable.

-- A variable is a function: from a map of variables to a Maybe Integer value.
-- A "variable" type is `:: (M.Map String Integer -> Maybe Integer)`

-- This answers the question: A "variable" has what value?
instance HasVars (M.Map String Integer -> Maybe Integer) where
	var varName = M.lookup varName


-- This answers the question: How to add/mul two variables?
instance Expr (M.Map String Integer -> Maybe Integer) where
	
	-- Goal:
	--   > add (lit 3) (lit 4) $ M.fromlist [("x", 6)]
	--   == Just 7
	-- We don't need the variable mapping to evaluate a literal value.
	lit n _ = Just n
	
	--------------

	-- Goal:
	--   > add (lit 3) (var "x") $ M.fromList [("x", 6)]
	--   == Just 9
	-- Notes:
	--   Must return a (M.Map String Integer -> Maybe Integer)
	--   varA, varB :: (M.Map String Integer -> Maybe Integer)
	-- Need to pass the mapping to subexpressions
	--   and appropriately combine results.
	
	-- Problem:
	--   Output type should be same as input type, both are complex.
	--   The variable-to-value mapping will be passed later.
	--   So, how do I split the variable-to-value mapping to give to
	--     both variables?
	--   Edit: Discovered I can add an extra param! Don't forget
	--       that point-free and extra param are very similar.
	
	--   How do we add two variables?
	add varA varB varMap =
		case (varA varMap) of
			Nothing -> Nothing
			Just a  -> case varB varMap of
				Nothing -> Nothing
				Just b  -> Just (a + b)

	------------------

	mul varA varB varMap =
		case (varA varMap) of
			Nothing -> Nothing
			Just a  -> case varB varMap of
				Nothing -> Nothing
				Just b  -> Just (a * b)


-- Solution from Arnav Sastry, GitHub user [arknave](https://github.com/arknave)
--   Link: https://github.com/arknave/cis194/blob/85150134a9599082048c3ea71070a6b68829580c/ch5/CalcVar.hs
-- He uses monads. I don't understand monads, so I'll try to not copy this answer.

--instance Expr (M.Map String Integer -> Maybe Integer) where
--	add a b m = liftM2 (+) (a m) (b m)
--	mul a b m = liftM2 (*) (a m) (b m)
--	lit a     = const $ Just a


-- Test desired behavior.
withVars :: [(String, Integer)]
			-> (M.Map String Integer -> Maybe Integer)
			-> Maybe Integer
withVars vs exp = exp $ M.fromList vs

-- > :t add (lit 3) (var "x")
-- add (lit 3) (var "x") :: (Expr a, HasVars a) => a
-- > withVars [("x", 6)] $ add (lit 3) (var "x")
-- Just 9
-- > withVars [("x", 6)] $ add (lit 3) (var "y")
-- Nothing
-- > withVars [("x", 6), ("y", 3)] $ mul (var "x") (add (var "y") (var "x"))
-- Just 54


