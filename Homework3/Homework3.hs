{-# OPTIONS_GHC -Wall #-}

module Golf where
import Data.List

-- Exercise 1: Hopscotch
{-

Your ﬁrst task is to write a function which outputs a list of lists.

The ﬁrst list in the output should be the same as the input list.
The second list in the output should contain every second element from the input list
. . .
and the nth list in the output should contain every nth element from the input list.

For example:
skips "ABCD" == ["ABCD", "BD", "C", "D"]
skips "hello!" == ["hello!", "el!", "l!", "l", "o", "!"]
skips [1] == [[1]]
skips [True,False] == [[True,False], [False]]
skips [] == []
Note that the output should be the same length as the input.


Algorithm development notes:

> skips "ABCDEF"

ABCDEF -- drop 0, keep 1, repeat OR drop len-6, keep 1, repeat
 B D F -- drop 1, keep 1, repeat OR drop len-5, keep 1, repeat
  C  F -- drop 2, keep 1, repeat OR drop len-4, keep 1, repeat
   D   -- drop 3, keep 1, repeat OR drop len-3, keep 1, repeat
    E  -- drop 4, keep 1, repeat OR drop len-2, keep 1, repeat
     F -- drop 5, keep 1, repeat OR drop len-1, keep 1, repeat

where
  all               == "ABCDEF"
  len == length all == 6


skips :: [a] -> [[a]]
Q: If 'a' has n elements, this function should have n lists.
How do we duplicate it n times?

A: Perform a function on input with argument,
  then perform it again with different argument.
To do this, we must introduce a parameter, n,
  which means "skip n elements".

-}



-- IRC ideas:
------

{-
-- Use complex tools:
let skips xs = map (\i -> every i xs) [1..length xs] where every k xs = xs^..Lens.elements (\j -> mod j k == k - 1) in skips "ABCDEF"
let skips xs = map every [1..length xs] where every k = xs^..Lens.elements (\j -> mod j k == k - 1)
let skips xs = [1..length xs] <&> \ k -> xs^..Lens.elements (\j -> mod j k == k - 1)

-- Interesting idea: use [Data.List.Split module](http://lambda.haskell.org/platform/doc/current/packages/split-0.2.2/doc/html/Data-List-Split.html#v:chunksOf)
map head . chunksOf 3 $ "ABCDEF"

-- This one looks simple:
let skips xs = skips' 0 where skips' n = case drop n xs of [] -> [] ; (first : rest) -> (first : every n rest) : skips' (n + 1) ; every n xs = case drop n xs of [] -> [] (first : rest) -> first : every n rest in skips "ABCDEF"

-- Expand that last one.
let skips xs = skips' 0
  where
    skips' n = case drop n xs of
      [] -> [] ;
      (first : rest) -> (first : every n rest) : skips' (n + 1) ;
    every n xs = case drop n xs of [] -> [] (first : rest) -> first : every n rest
in skips "ABCDEF"
-}




-- My version: counting down.
------

-- Start from last element. This means skips' is more readable.
skips :: [a] -> [[a]]
skips xs = skips' (length xs - 1) xs -- We need a counter, so make a new function. (reverse curry?)

-- Keep every nth element of xs.
skips' :: Int -> [a] -> [[a]]
skips' 0 xs = [xs] -- If skip 0, then no change.
skips' _ [] = [[]] -- For completeness.
skips' n xs = skips' (n-1) xs ++ [skipsN n xs]
--skips' n xs = skips' (n-1) xs ++ [every (n+1) xs]


-- IRC version: counting up.
------

-- Start from zero. This means skipsB' is more compact.
skipsB :: [a] -> [[a]]
skipsB xs = skipsB' 0 xs -- Skip 0, then 1, ..., until skipping every element.

-- Keep every nth element of xs.
skipsB' :: Int -> [a] -> [[a]]
skipsB' _ [] = [[]] -- For completeness.
skipsB' n xs
  | n >= length xs = [] -- Stop recursion if skipping every element.
  | otherwise      = skipsN n xs : skipsB' (n + 1) xs
  -- | otherwise      = every (n+1) xs : skipsB' (n + 1) xs



-- Two methods to print every xth element:
-- 1) Skip n elements, then print.
-- 2) Retrieve nth element.
------

-- 1) Skip n elements, then print char. (Imperative mindset?)
skipsN :: Int -> [a] -> [a]
skipsN _ [] = []
skipsN 0 xs = xs
skipsN n xs = takeNth n xs ++ skipsN n (drop (n+1) xs)
  where takeNth m ys = take 1 (drop m ys)
{-
> skipsN 0 "ABCD" == "ABCD"
> skipsN 1 "ABCD" == "BD"
> skipsN 2 "ABCD" == "C"
> skipsN 3 "ABCD" == "D"
> skipsN 4 "ABCD" == ""
-}

-- 2) Filter every nth letter from the string. (Functional mindset?)
every :: Int -> [a] -> [a]
every n xs = case drop (n - 1) xs of -- Use case to perform an action,
                                      -- then decide what to do based on result.
    (y : ys) -> y : every n ys -- After dropping, recurse until end of list.
    []       -> [] -- If drop returned empty list, we hit end of list.
{-
> skipsN 0 "ABCD" == "ABCD"
> skipsN 1 "ABCD" == "ABCD"
> skipsN 2 "ABCD" == "BD"
> skipsN 3 "ABCD" == "C"
> skipsN 4 "ABCD" == "D"
> skipsN 5 "ABCD" == ""
-}




-- Exercise 2: Local Maxima
{-

A local maximum of a list is an element of the list which is strictly
greater than both the elements immediately before and after it.

Write a function which returns, in order, all the local maxima in the input list.

For example:
localMaxima [2,9,5,6,1] == [9,6]
localMaxima [2,3,4,1,5] == [4]
localMaxima [1,2,3,4,5] == []

-}

localMaxima :: [Integer] -> [Integer]
localMaxima (a : b : c : xs)
  | b > a && b > c  =  b : localMaxima (c : xs) -- b is maxima, so add it to the list.
                                                -- c can not be maxima because it's less than b.
  | otherwise       =  localMaxima (b : c : xs) -- b is not maxima, but maybe c is.
localMaxima [] = []
localMaxima _  = []



-- Exercise 3: Histogram
{-

For this task, write a function which takes a list of Integers between 0 and 9
and outputs a vertical histogram, which shows the occurrences of each number.

> histogram [1,1,1,5] ==
 *
 *
 *   *
==========
0123456789

> histogram [1,4,5,4,6,6,3,4,2,4,9] ==
    *
    *
    * *
 ******  *
==========
0123456789

Important note: If you type something like histogram [3,5] at
the ghci prompt, you should see something like this:
" * * \n==========\n0123456789\n"
Use `putStr (histogram [3,5])` to convert newline characters in ghci output.

-}


{-
Notes:

A)
histogram [1] ==
 *
==========
0123456789

== *        \n=========\n0123456789\n

== <1 * Row> ++ "\n=========\n0123456789\n"

B)
histogram [1,1,1] ==
 *
 *
 *
==========
0123456789

== *        \n *        \n *        \n=========\n0123456789\n

== <n * Row> ++ "\n" ++ "=========\n0123456789\n"
  where n = countMaxOccurrences

C)
histogram [1,1,1,2] ==
 *
 *
 **
==========
0123456789

== *        \n *        \n **       \n=========\n0123456789\n

== <n * Row> ++ "\n" ++ "=========\n0123456789\n"
  where
    n = countMaxOccurrences
    i = countCurrentOccurrence
    Row = numberWith

Idea:
For asterick rows,
Make bottom row first.
  - If input array has a certain number, n, remove it and add an asterick.
  - Pass remaining number array to this same function.
  - Will be recursive.

-}

histogram :: [Integer] -> String
histogram xs = occurrenceTable xs ++ "\n==========\n0123456789\n"

-- The domain of our histogram. All other values are ignored.
nDomain :: [Integer]
nDomain = [0,1,2,3,4,5,6,7,8,9]

-- Make the data portion of the histogram.
-- > occurrenceTable [1,1,1,2] == "\n *        \n *        \n **       "
occurrenceTable :: [Integer] -> String
occurrenceTable [] = ""
occurrenceTable xs = occurrenceTable (remainingXs) ++ "\n" ++ astRow cleanXs
  where
    remainingXs = removeOneOfEach cleanXs
    cleanXs = xs `intersect` nDomain

-- Print a row of astericks.
-- Replace the first occurrence of each n in xs with a an asterick, where n is from nDomain.
-- > astRow [1,1,1,2] == " **      "
astRow :: [Integer] -> String
astRow [] = ""
astRow xs = map xsHasN nDomain -- For each n in nDomain, check if it exists in xs
  where
    xsHasN n = if (n `elem` xs) then '*' else ' '

-- Remove the first occurrence of each n from nDomain.
-- > removeOneOfEach [1,1,1,2] == [1,1]
removeOneOfEach :: [Integer] -> [Integer]
removeOneOfEach xs = xs \\ nDomain
--removeOneOfEach xs = deleteFirstsBy (==) xs nDomain
-- (\\) is like deleteFirstsBy, but uses a default equality function.
-- Reading more, (\\) is a "Set Difference" operator. B / A = {x E B | x !E A}
-- B \ A == Get everything from B which is not in A. It's like B - A.





