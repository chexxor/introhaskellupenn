{-# OPTIONS_GHC -Wall #-}

module Homework4 where
import Data.List(genericSplitAt)

-- Exercise 1: Wholemeal Programming
{-

Reimplement the following functions in idiomatic Haskell style:
- wholemeal programming
- pipeline of incremental transformations

Name your functions fun1' and fun2'
-}

fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x:xs)
	| even x = (x - 2) * fun1 xs
	| otherwise = fun1 xs
-- Notes:
-- Ignore odd numbers, fold with function.


fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
	| even n = n + fun2 (n `div` 2)
	| otherwise = fun2 (3 * n + 1)
-- Notes:
-- > f 15
-- [(15), 46, (23), 70, (35), 106, (53), 160, 80, 40, 20, 10, (5), 16, 8, 4, 2, 1, 0]
-- Make a list, sum only the even numbers.

{-
Hint: You may wish to use the functions `iterate` and `takeWhile`.

-}

fun1' :: [Integer] -> Integer
fun1' = foldr (\x y -> (x-2) * y) 1 . filter even


fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (>1) . iterate (\x -> if even x then x `div` 2 else 3 * x + 1)


-- Exercise 2: Folding with Trees
{-

Recall the deﬁnition of a binary tree data structure.
The height of a binary tree is the length of a path
from the root to the deepest node.

A binary tree is balanced if the height of its left and right
subtrees differ by no more than 1, and its left and right subtrees are
also balanced.

For this exercise, write a function called `foldTree`
which generates a balanced binary tree from a list of values using `foldr`.

For example:

foldTree "ABCDEFGHIJ" ==
	Node 3
		(Node 2
			(Node 0 Leaf ’F’ Leaf)
			’I’
			(Node 1 (Node 0 Leaf ’B’ Leaf) ’C’ Leaf))
		’J’
		(Node 2
			(Node 1 (Node 0 Leaf ’A’ Leaf) ’G’ Leaf)
			’H’
			(Node 1 (Node 0 Leaf ’D’ Leaf) ’E’ Leaf))

The nodes aren't required to be in the same exact order.

-}

data Tree a = Leaf
	| Node Integer (Tree a) a (Tree a)
	deriving (Show, Eq)


foldTree :: [a] -> Tree a
foldTree = foldr treeFunc Leaf -- What's a better name for `treeFunc`?


-- Remember with `foldr`:
-- the foldFunction will see the right-most element first.

treeFunc :: a -> Tree a -> Tree a
treeFunc n Leaf = Node 0 Leaf n Leaf
treeFunc n (Node _ l m r) =
		if dl >= dr
			then (Node (dr+1) l m (treeFunc n r)) -- Add new element to right.
			else (Node (dl+1) (treeFunc n l) m r) -- Add new element to left.
		where
			dl = depth l
			dr = depth r


depth :: Tree a -> Integer
depth Leaf = 0
depth (Node _ l _ r) = 1 + max (depth l) (depth r)


-- Test data

testTree :: Tree Char
testTree = (Node 0 Leaf 'A' Leaf)

testTreeb :: Tree Char
testTreeb = (Node 2 Leaf 'B' Leaf)

testTree2 :: Tree Char
testTree2 = (Node 1 testTree 'C' testTreeb)

testTree3 :: Tree Char
testTree3 = (Node 2 testTree2 'D' Leaf)

testTree4 :: Tree Char
testTree4 = (Node 3 testTree3 'E' Leaf)



-- Exercise 3: More Folds!
{-

1. Implement a function `xor` using a fold.
It should return True if and only if there are an odd number
of True values contained in the input list.
It does not matter how many False values the input list contains.

For example:

xor [False, True, False] == True
xor [False, True, False, False, True] == False

2. Implement map as a fold.

-}

-- 1)
xor :: [Bool] -> Bool
-- Remove the Falses, then flip the accumulator on each element.
xor = foldr (\_ y -> not y) False . filter (==True)


-- 2)
map' :: (a -> b) -> [a] -> [b]
map' f xs = foldr func [] xs
	where func x y = f x : y -- Accumulate a list of results to return.

-- > import Test.QuickCheck
-- > quickCheck (\xs -> map' (+1) xs == map (+1) xs)
-- +++ OK, passed 100 tests.

{-
3. (Optional) Implement foldl using foldr.

Complete the myFoldl definition such that it behaves
identically to the standard foldl function.

myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr ...

Hint: Study how the application of foldr and foldl work out:
foldr f z [x1, x2, ..., xn] == x1 `f` (x2 `f` ... (xn `f` z)...)
foldl f z [x1, x2, ..., xn] == (...((z `f` x1) `f` x2) `f`...) `f` xn

-}

myFoldl :: (a -> b -> a) -> a -> [b] -> a
-- This seems to work...
myFoldl f base xs = foldr (flip f) base (reverse xs)

-- HaskellWiki answer also works: http://www.haskell.org/haskellwiki/Foldl_as_foldr
--myFoldl f base xs = foldr (\b g x -> g (f x b)) id xs base

-- But I don't understand the "id" function part,
--   and this quick, intuitive answer doesn't work.
--myFoldl f base xs = foldr (\x g -> (f g x)) base xs


-- > import Test.QuickCheck
-- > quickCheck (\xs -> myFoldl (\x y -> max x y + y) 1 xs == foldl (\x y -> max x y + y ) 1 xs)
-- +++ OK, passed 100 tests.


-- Exercise 4: Finding primes

{-

Read about the Sieve of Sundaram, which calculates all prime numbers
up to a specified integer.
http://en.wikipedia.org/wiki/Sieve_of_Sundaram

Implement this algorithm using function composition.
Given an integer n, your function should generate
all the odd prime numbers up to 2n + 2.

Hint: Use the following function, which computes the Cartesian product of two lists.
This is similar to zip, but it produces all possible pairs instead
of matching up the list elements.

For example:
cartProd [1,2] [’a’,’b’] == [(1,’a’),(1,’b’),(2,’a’),(2,’b’)]

-}

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

{-
Algorithm:
* Start with a list of the integers from 1 to n.
* Remove all numbers of the form i + j + 2ij where:
    i,j in N, 1 <= i <= j
    i + j + 2ij <= n
* Double all numbers and increment by one.
* This is a list of odd prime numbers below 2n + 2.
-}

--sieveSundaram :: Integer -> [Integer]
--sieveSundaram = map (\x -> 2*x+2) . filter keepPred . enumFromTo 1
--	where
		--keepPred :: Integer -> Bool
		--keepPred x = x `notElem` exclusionNumbers
		--exclusionNumbers :: Integer -> [Integer]
		--exclusionNumbers = takeWhile (<=n) . sort $ [i+j+2*i*j | i <- [1..n], j <- [i..n]]

{-

------------
Solve it by hand.

input = [1,2,3,4,5,6,7,8,9,10]
n = 10

Remove which ones?
f(i,j) = i+j+2*i*j

i  j  f(i,j)
1  1  4
2  1  7
2  2  12 -- err? shouldn't be even numbers
3  1  10
3  2  17
3  3  24
4  1  13
4  2  22
4  3  31
4  4  40
5  1  16
5  2  27
5  3  38
5  4  49

exhaustiveList = [4, 7, 12, 10, 17, 24, 13, 22, 31, 40, 16, 27, 38, 49]
sort exhaustiveList
== [4,7,10,12,13,16,17,22,24,27,31,38,40,49]

input \\ exclusions
== [1,2,3,5,6,8,9]

doubleAndIncrement = map ((+1) . (*2))

doubleAndIncrement $ input \\ exclusions
== [3,5,7,11,13,17,19]

exclusions = takeWhile (<=10) $ sort exhaustiveList
== [4,7,10]

That list looks ok.


--------------
Problem: How to generate exclusions when we don't know n?

Idea:

exclusions :: Integer -> [Integer]
exclusions = map (\(i,j) -> i+j+2*i*j) ijCombos
== [4,7,10,13,16,19,22,25,28,31,7,12,17,22,27,32,37,42,...]

Where:

ijCombos :: [(Integer, Integer)]
ijCombos = cartProd [1..n] [1..n]
== [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),
(1,10),(2,1),(2,2),(2,3),(2,4),(2,5),...]

That list looks ok.

--------------
Problem: We need a functional form of ijCombos,
because [1..n] [1..n] is not functional.

Idea:

We should look at the cartProd inputs.

-- Do something like this: cartProd (curry ([1..10],[1..10]))

ijCombos :: Integer -> [(Integer, Integer)]
ijCombos = (uncurry cartProd) $ pairEnumFromTo
== [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),
(1,10),(2,1),(2,2),(2,3),(2,4),(2,5),...]


Where:

pairEnumFromTo :: Integer -> ([Integer], [Integer])
pairEnumFromTo x = splitAt (x) $ concat $ replicate 2 $ enumFromTo 1 x
-- Similar to: pairEnumFromTo x = (enumFromTo 1 x, enumFromTo 1 x)

That list looks ok.

--------------
Problem: We only need j>=i (e.g. (2,1) is not needed).

Use a simple filter:

ijCombosFiltered :: Integer -> [(Integer, Integer)]
ijCombosFiltered = filter (\(i,j) -> j>=i) ijCombos
== [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),
(1,10),(2,2),(2,3),...]

Nice!

-}


-- This is rather slow, due to creating a new exclusion list for each element.
-- This is the slow part: filter (\x -> x `notElem` exclusions x)
-- TODO: How to make that predicate faster?
sieveSundaram :: Integer -> [Integer]
sieveSundaram = (2:) . map (\x -> 2*x+1) . filter (\x -> x `notElem` exclusions x) . enumFromTo 1

exclusionNums :: [Integer]
exclusionNums = exclusions 10

exclusions :: Integer -> [Integer]
exclusions = map (\(i,j) -> i+j+2*i*j) . ijCombos

ijCombos :: Integer -> [(Integer, Integer)]
ijCombos = filter (\(i,j) -> j>=i) . (uncurry cartProd) . pairEnumFromTo

pairEnumFromTo :: Integer -> ([Integer], [Integer])
pairEnumFromTo x = genericSplitAt x . concat . replicate 2 $ enumFromTo 1 x



-- StackOverflow answer:
-- http://stackoverflow.com/questions/16246456/sieve-of-sundaram-list-comprehension
sSundDelete :: Integer -> [Integer]            
sSundDelete n = [i+j+2*i*j|i<-[1..n], j<-[i..n]]

sSund :: Integer -> [Integer]
sSund n = let del = sSundDelete n in
     2:[2*x+1 | x <- [1..n], not (x `elem` del)]


-- > quickCheck (\x -> sieveSundaram x == sSund x)
-- +++ OK, passed 100 tests.

