{-# OPTIONS_GHC -Wall #-}

{-

Implement the validation algorithm for
credit cards.

It follows these steps:

1) Double the value of every second digit beginning from the right.
	the last digit is unchanged;
	the second-to-last digit is doubled;
	the third-to-last digit is unchanged;
	and so on.
	For example, [1,3,8,6] becomes [2,3,16,6].

2) Add the digits of the doubled values
	and the undoubled digits from the original number.
	For example, [2,3,16,6] becomes 2+3+1+6+6 = 18.

3) Calculate the remainder when the sum is divided by 10.
	For the above example, the remainder would be 8.
	If the result equals 0, then the number is valid.

The solution is broken into several "exercises".

-}

-- Exercise 1:
-- Find the digits of the number.

-- BELOW - just for fun

-- numDigits 234 == 3
numDigits :: Integer -> Integer
numDigits n
  | n < 0            = 0
  | n >= 0 && n <= 9 = 1
  | otherwise        = 1 + numDigits (n `div` 10)

-- headDigit 234 == 2
headDigit :: Integer -> Integer
headDigit n
  | n < 0            = 0
  | n >= 0 && n <= 9 = n
  | otherwise        = headDigit (n `div` 10)

-- popDigit 234 == 34
-- popDigit 1034 == 34 -- Note!
popDigit :: Integer -> Integer
popDigit n
  | n < 0            = n
  | n >= 0 && n <= 9 = n
  | otherwise        = n - oneSigDigit n

-- tailDigits 234 == [3,4]
tailDigits :: Integer -> [Integer]
tailDigits n
  | n < 0            = []
  | n >= 0 && n <= 9 = [n]
  | otherwise        = toDigits (popDigit n)

-- oneSigDigit 234 == 200
oneSigDigit :: Integer -> Integer
oneSigDigit n
  | n < 0            = n
  | n >= 0 && n <= 9 = n
  | otherwise        = headDigit n * 10 ^ ((numDigits n) - 1)

-- initDigits 234 == [2,3]
initDigits :: Integer -> [Integer]
initDigits n
  | n < 0             = []
  | n >= 0 && n <= 9  = []
  | otherwise         = headDigit n : [headDigit (popDigit 234)]

-- ABOVE - just for fun

-- lastDigit 123 == 3
lastDigit :: Integer -> Integer
lastDigit n
  | n < 0            = n
  | n >= 0 && n <= 9 = n
  | otherwise        = n `mod` 10

-- Same as toDigits, but in reverse.
toDigitsRev :: Integer -> [Integer]
toDigitsRev n
  | n <= 0           = []
  | n >= 1 && n <= 9 = [n]
  | otherwise        = lastDigit n : toDigitsRev (n `div` 10)

-- Converts positive Integers to a list of digits.
-- toDigits 234 == [2,3,4]
toDigits :: Integer -> [Integer]
toDigits n = reverse (toDigitsRev n)



-- Exercise 2:
-- Once we have the digits in the proper order,
--   we need to double every other one.
-- The algorithm should start from the right. That is,
--   second-to-last, fourth-to-last, etc.

-- doubleEveryOther' [2,2,2,2,2,2] => Should make [2,4,2,4,2,4]
doubleEveryOther' :: [Integer] -> [Integer]
doubleEveryOther' []       = []
doubleEveryOther' [x]      = [x]
doubleEveryOther' (x:y:xs) = x : 2*y : doubleEveryOther' xs

-- doubleEveryOther [2,2,2,2,2,2] => Should make [4,2,4,2,4,2]
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = reverse . doubleEveryOther' . reverse



-- Exercise 3:
-- The output of doubleEveryOther has a mix of one-digit
--   and two-digit numbers.
-- Deﬁne a `sumDigits` function to calculate the sum of all digits:

-- sumDigits [16,7,12,5] => 1 + 6 + 7 + 1 + 2 + 5 => 22
sumDigits :: [Integer] -> Integer
sumDigits []         = 0
sumDigits [x]
  | x < 0            = 0
  | x >= 0 && x <= 9 = x
  | otherwise        = sumDigits (toDigits x)
sumDigits (x:xs)
  | x < 0            = 0
  | x >= 0 && x <= 9 = x + (sumDigits (xs))
  | otherwise        = sumDigits (toDigits x) + (sumDigits (xs))



-- Exercise 4:
-- Define a function which indicates whether an Integer
--   could be a valid credit card number.
-- This will use all functions deﬁned in the previous exercises.

-- validate 4012888888881881 => True
-- validate 4012888888881882 => False
validate :: Integer -> Bool
validate x = sumDigits (doubleEveryOther(toDigits x)) `mod` 10 == 0




-- Exercise 5
{-

The Towers of Hanoi is a classic puzzle with a recursive solution.
Disks of different sizes are stacked on three pegs;
the goal is to get from a starting conﬁguration,
  with all disks stacked on the ﬁrst peg
to an ending conﬁguration
  with all disks stacked on the last peg.

The only rules are
- you may only move one disk at a time
- a larger disk may never be stacked on top of a smaller one

For example, as the ﬁrst move all you can do is move the topmost,
smallest disk onto a different peg, since only one disk may be moved
at a time.

From this point, it is illegal to put the green disk on top of
the smaller blue one.

To move n discs (stacked in increasing size) from peg a to peg b
using peg c as temporary storage,

1) move n - 1 discs from a to c using b as temporary storage
2) move the top disc from a to b
3) move n - 1 discs from c to b using a as temporary storage.

For this exercise, deﬁne a function hanoi with the following type:

type Peg = String
type Move = (Peg, Peg)
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]

Given the number of discs and names for the three pegs,
hanoi should return a list of moves to be performed to move the stack of
discs from the ﬁrst peg to the second.

-}

type Peg = String -- type synonym. Peg and String are interchangeable
type Move = (Peg, Peg)
-- hanoi 1 "a" "b" "c" == [("a","b")]
-- hanoi 2 "a" "b" "c" == [("a","c"), ("a","b"), ("c","b")]
-- hanoi 3 "a" "b" "c" == [("a","b"), ("a","c"), ("b","c"), ("a","b"), ("c","a"), ("c","b"), ("a","b")]
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
-- n => number of discs, a,b,c => peg names
hanoi 0 _ _ _ = [] -- If a parameter in unused, use '_'. Why? Only because it looks better?
hanoi 1 a b _ = [(a, b)]
hanoi n a b t =
    hanoi (n - 1) a t b
    ++ [(a, b)]
    ++ hanoi (n - 1) t b a


--Exercise 6 (Optional)
{-

What if there are four pegs instead of three?

The goal is the same, but now there are two pegs that can be used as “temporary” storage.

Write a function, similar to hanoi, which solves this problem in as few moves as possible.
It should be possible to do it in far fewer moves than with three pegs.

For example,
with three pegs it takes 2^15 - 1 = 32767 moves to transfer 15 discs.
With four pegs it can be done in 129 moves.

-}

-- hanoi4 0 "a" "b" "c" "d" == []
-- hanoi4 1 "a" "b" "c" "d" == [("a","b")]
-- hanoi4 2 "a" "b" "c" "d" == [("a","c"), ("a", "b"), ("c","b")]
-- hanoi4 3 "a" "b" "c" "d" == [("a","c"), ("a","d"), ("a","b"), ("d","b"), ("c","b")]
hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
-- n => number of discs, a,b,c,d => 4 pegs names
hanoi4 0 _ _ _ _ = []
hanoi4 1 a b _ _ = [(a, b)]
hanoi4 2 a b t _ = hanoi4 1 a t "a" "b" ++ [(a, b)] ++ hanoi4 1 t a "c" "d"
hanoi4 n a b h l = 
    hanoi topHalf a h b -- Move top half to higher-than pin
    ++ hanoi botHalf a b l -- Move bottom half to lower-than pin
    ++ hanoi topHalf h b a
    where
      topHalf = (n `div` 2)
      botHalf = (n - (n `div` 2))

{-
> length (hanoi4 15 "a" "b" "c" "d")
509 -- Not 129 moves, as teacher says is possible, but not bad. ^_^
-}

{-
> hanoi4 4 "a" "b" "c" "d"
[("a","b"),("a","c"),("b","c"),("a","d"),("a","b"),
("d","b"),("c","a"),("c","b"),("a","b")]

Proof:
1 0 0 0
2 1 0 0
3 1 2 0
3 0 1 0
4 0 1 3
0 4 1 3
0 3 1 0
1 3 2 0
1 2 0 0
0 1 0 0
-}


{-
> hanoi4 5 "a" "b" "c" "d"
[("a","b"),("a","c"),("b","c"),("a","b"),("a","d"),
("b","d"),("a","b"),("d","a"),("d","b"),("a","b"),
("c","a"),("c","b"),("a","b")]

Proof:
1 0 0 0
2 1 0 0
3 1 2 0
3 0 1 0
4 3 1 0
5 3 1 4
5 0 1 3
0 5 1 3
3 5 1 4
3 4 1 0
0 3 1 0
1 3 2 0
1 2 0 0
0 1 0 0
-}


{-
> hanoi4 6 "a" "b" "c" "d"
[("a","c"),("a","b"),("c","b"),("a","c"),("b","a"),
("b","c"),("a","c"),("a","b"),("a","d"),("b","d"),
("a","b"),("d","a"),("d","b"),("a","b"),("c","b"),
("c","a"),("b","a"),("c","b"),("a","c"),("a","b"),
("c","b")]

Proof:
1 0 0 0
2 0 1 0
3 2 1 0
3 1 0 0
4 1 3 0
1 2 3 0
1 0 2 0
4 0 1 0
5 4 1 0
6 4 1 5
6 0 1 4
0 6 1 4
4 6 1 5
4 5 1 0
0 4 1 0
0 1 2 0
2 1 3 0
1 4 3 0
1 3 0 0
2 3 1 0
0 2 1 0
0 1 0 0
-}



-----------
-- Solution below uses "Real Math Algorithm", for n discs and many pegs.
-- Link: http://stackoverflow.com/questions/3607161/towers-of-hanoi-with-k-pegs
-- User: http://stackoverflow.com/users/423105/larsh
-----------

-- hanoi for n disks and r pegs [p1, p2, ..., pr]
hanoiR :: Integer -> [a] -> [(a, a)]

-- zero disks: no moves needed.
hanoiR 0 _ = []

-- These three are my addition, to complete pattern.
hanoiR _ [] = []
hanoiR _ [_] = []
hanoiR _ [_, _] = []

-- one disk: one move and two pegs needed.
hanoiR 1 (p1 : p2 : _) = [(p1, p2)] -- only needed for smart-alecks?

-- n disks and r > 3 pegs: use Frame-Stewart algorithm
hanoiR n (p1 : p2 : p3 : rest) =
    hanoiR k (p1 : p3 : p2 : rest) ++
    hanoiR (n - k) (p1 : p2 : rest) ++
    hanoiR k (p3 : p2 : p1 : rest)
    where
      k
        | (null rest)   = n - 1
        | otherwise     = n `div` 2


{-

Comparison:
hanoiR = larsh
hanoi4 = Me
> length (hanoiR 4 ['a', 'b', 'c', 'd'])
9
> length (hanoi4 4 "a" "b" "c" "d")
9

> length (hanoiR 5 ['a', 'b', 'c', 'd'])
13
> length (hanoi4 5 "a" "b" "c" "d")
13

> length (hanoiR 6 ['a', 'b', 'c', 'd'])
17
> length (hanoi4 6 "a" "b" "c" "d")
21

> length (hanoiR 15 ['a', 'b', 'c', 'd'])
305
> length (hanoi4 15 "a" "b" "c" "d")
509
-}
